# Environment logger Microcontroller Firmware 0.1.0

Firmware for the the sensor logger based on the Adafruit Feather M0 WiFi - ATSAMD21 + ATWINC1500. The device measures visible light (TEMT6000), co2 and volatile organic compounds(CCS811), temperature, humidity(AHT10) and noise (I2S-SPH0645LM4H).

![Device diagram](diagram/wiring_diagram.png)
