#include <Arduino.h>
#include "data/types.h"
#include "sensors/sensors.h"
    
void setup() {
  Serial.begin(115200);
  while(!Serial) delay(10);
  setupSensors();
}

void loop() {
  samplingScheduler();
}