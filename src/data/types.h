#ifndef __APP_DATA
#define __APP_DATA
#include <Arduino.h>

typedef enum s
{
  // Available states for sampling scheduler
  sample_temperature,
  sample_co2,
  sample_light,
  sample_audio
} State;

// Stores application status at any given time
struct Application
{
  State state = sample_temperature;
};
Application app;

struct Tempdata
{
  // Sensors' samples are stored in a set of Circular arrays in order to allow result averaging
  static const uint8_t NUM_MEASUREMENTS = 10;

  // Temperature/humidity sensor temporary data
  float temperature_samples[NUM_MEASUREMENTS] = {}; // Array of temperature samples
  uint8_t temperature_samples_pos = 0;              // Current position of insertion in the array
  boolean temperature_samples_full = false;         // is the array full? used to calculate average

  float humidity_samples[NUM_MEASUREMENTS] = {};
  uint8_t humidity_samples_pos = 0;
  boolean humidity_samples_full = false;

  // Co2/tvoc sensor temporary data
  float co2_co2_samples[NUM_MEASUREMENTS] = {};
  uint8_t co2_co2_samples_pos = 0;
  boolean co2_co2_samples_full = false;

  float co2_tvoc_samples[NUM_MEASUREMENTS] = {};
  uint8_t co2_tvoc_samples_pos = 0;
  boolean co2_tvoc_samples_full = false;

  // Light sensor temporary data
  float light_samples[NUM_MEASUREMENTS] = {};
  uint8_t light_samples_pos = 0;
  boolean light_samples_full = false;
};
Tempdata tdata;

#endif
