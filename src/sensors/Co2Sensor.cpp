#include "Co2Sensor.h"

Adafruit_CCS811 ccs;

Co2Sensor::Co2Sensor()
{
}

void Co2Sensor::begin()
{
  if (!ccs.begin())
    Serial.println("Failed to start CCS811 sensor! Please check your wiring.");
}

float *Co2Sensor::getSample()
{
  static float result[3];
 
  if (ccs.available())
  {
    if (!ccs.readData())
    {
      
      result[0] = ccs.calculateTemperature();
      result[1] = ccs.geteCO2();
      result[2] = ccs.getTVOC();
    }
  }
  return result;
}
