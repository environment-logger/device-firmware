#include "SoundSensor.h"

Adafruit_ZeroI2S i2s;

SoundSensor::SoundSensor()
{
}

void SoundSensor::begin()
{
  i2s = Adafruit_ZeroI2S();
  i2s.begin(I2S_32_BIT, 44100);
  i2s.enableRx();
}

void SoundSensor::getSample()
{
  int32_t left[BUFSIZE];
  int32_t right[BUFSIZE];
  uint32_t time = 0;
  uint32_t startpoint = micros();
  for (int i = 0; i < BUFSIZE; i++)
  {
    while (!i2s.rxReady())
    {
    }
    i2s.read(&left[i], &right[i]);
  }
  time = micros() - startpoint;
  for (int i = 0; i < BUFSIZE; i++)
  {
    if (left[i] < -1)
    {
      left[i] = left[i] >> 14;
    }
    else
    {
      left[i] = right[i] >> 14;
    }
    Serial.print(left[i]);
    Serial.print(",");
  }
  // tiempo de sampleo en microsegundos, el salto de linea cierra el ciclo de medidas
  Serial.println(time);
}

