/*
  LightSensor.h - Library for interfacing TEMT600 sensor on nvlog.
  Created by Pablo Mariño Boga, June 23, 2021.
*/
#ifndef __LightSensor_h
#define __LightSensor_h

#include <Arduino.h>

class LightSensor
{

private:
public:
  LightSensor();
  float getSample();
};

#endif