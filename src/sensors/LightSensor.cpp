#include "LightSensor.h"

LightSensor::LightSensor()
{
}

float LightSensor::getSample()
{
  const uint8_t SENSOR_PIN_LIGHT = A2;
  const float TEMT6000_IN_VOLT = 3.3;

  float value = analogRead(SENSOR_PIN_LIGHT);
  float volts = (value * TEMT6000_IN_VOLT) / 1024.0;
  float amps = volts / 10000.0;
  float microamps = amps * 1000000;
  float lux = microamps * 2.0;

  return lux;
}
