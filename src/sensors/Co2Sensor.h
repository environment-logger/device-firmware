/*
  Co2Sensor.h - Library for interfacing CCS811 sensor on nvlog.
  Created by Pablo Mariño Boga, January 20, 2022.
*/

#ifndef __Co2Sensor_h
#define __Co2Sensor_h

#include <Arduino.h>
#include "Adafruit_CCS811.h"

class Co2Sensor
{

private:
  Adafruit_CCS811 ccs;
  float *result;

public:
  Co2Sensor();
  void begin();
  float *getSample();
};

#endif