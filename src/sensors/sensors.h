#include "../data/types.h"
#include "Co2Sensor.h"
#include "LightSensor.h"
#include "TemperatureSensor.h"
#include "SoundSensor.h"

TemperatureSensor temp_sensor;
LightSensor light_sensor;
Co2Sensor co2_sensor;
SoundSensor sound_sensor;

/**
 * @brief Sensor's initialization
 *
 */
void setupSensors()
{
    temp_sensor.begin();
    co2_sensor.begin();
    sound_sensor.begin();
}
/**
 * @brief Stores sensor's measurements in a circular array
 *
 * @param value
 * @param array_samples
 * @param array_pos
 * @param array_full
 * @param array_size
 */
void storeSample(float value, float *array_samples, uint8_t *array_pos, boolean *array_full, uint8_t array_size)
{
    array_samples[(*array_pos)] = value;// Inserto en la posicion que indica
    if (++(*array_pos) >= array_size) // incremento la posicion, si es la ultima vuelvo al principio
    {
        *array_pos = 0;
        if (!*array_full)
            *array_full = true; // Indica si el array esta lleno y puedo utilizar todos los valores para calcular la media, sino uso pos valores
    }
}
/**
 * @brief returns the Average value of a set of samples
 *
 * @param array_samples circular array of samples
 * @param array_pos array's current position of insertion
 * @param array_full wether the array is filled or not
 * @param array_size size of the array
 * @return float
 */
float getAverage(float *array_samples, uint8_t *array_pos, boolean *array_full, uint8_t array_size)
{

    uint8_t avsize; // Cuantos valores del array vamos a utilizar para la media
    if (*array_full)
        avsize = array_size;
    else
        avsize = (*array_pos);

    // Calculo la media aritmetica
    float result = 0;
    for (uint8_t i = 0; i < avsize; i++)
    {
        result = result + array_samples[i];
    }
    result = result / avsize;

    return result;
}

void samplingScheduler()
{
    static float *_temperature;
    static float *_co2;
    static float _light = 0;
    switch (app.state)
    {
        case sample_temperature:
        {
            _temperature = temp_sensor.getSample();
            storeSample(_temperature[0], tdata.temperature_samples, &tdata.temperature_samples_pos, &tdata.temperature_samples_full, tdata.NUM_MEASUREMENTS);
            storeSample(_temperature[1], tdata.humidity_samples, &tdata.humidity_samples_pos, &tdata.humidity_samples_full, tdata.NUM_MEASUREMENTS);
            // Envio a traves del serial la media de los datos almacenados de temperatura y humedad
            Serial.print(getAverage(tdata.temperature_samples, &tdata.temperature_samples_pos, &tdata.temperature_samples_full, tdata.NUM_MEASUREMENTS));
            Serial.print(",");
            Serial.print(getAverage(tdata.humidity_samples, &tdata.humidity_samples_pos, &tdata.humidity_samples_full, tdata.NUM_MEASUREMENTS));
            Serial.print(",");
            
            app.state = sample_co2;
            
            break;
        }
        case sample_co2:
        {
            _co2 = co2_sensor.getSample();
            storeSample(_co2[1], tdata.co2_co2_samples, &tdata.co2_co2_samples_pos, &tdata.co2_co2_samples_full, tdata.NUM_MEASUREMENTS);
            storeSample(_co2[2], tdata.co2_tvoc_samples, &tdata.co2_tvoc_samples_pos, &tdata.co2_tvoc_samples_full, tdata.NUM_MEASUREMENTS);
            // Envio a traves del serial la media de los datos almacenados de co2 y tvoc
            Serial.print(getAverage(tdata.co2_co2_samples, &tdata.co2_co2_samples_pos, &tdata.co2_co2_samples_full, tdata.NUM_MEASUREMENTS));
            Serial.print(",");
            Serial.print(getAverage(tdata.co2_tvoc_samples, &tdata.co2_tvoc_samples_pos, &tdata.co2_tvoc_samples_full, tdata.NUM_MEASUREMENTS));
            Serial.print(",");
            
            app.state = sample_light;
            break;
        }
        case sample_light:
        {
            _light = light_sensor.getSample();
            storeSample(_light, tdata.light_samples, &tdata.light_samples_pos, &tdata.light_samples_full, tdata.NUM_MEASUREMENTS);
            // Envio a traves del serial la media de los datos almacenados de co2 y tvoc
            Serial.print(getAverage(tdata.light_samples, &tdata.light_samples_pos, &tdata.light_samples_full, tdata.NUM_MEASUREMENTS));
            Serial.print(",");
            // Cambio de estado
            app.state = sample_audio;
            break;
        }
        case sample_audio:
        {
            // Obtengo las medidas, las envio a traves del serial y cierro linea
            sound_sensor.getSample();
            app.state = sample_temperature;
            break;
        }
        default:
        {
            break;
        }
    }
}