/*
  TemperatureSensor.h - Library for interfacing AHTX0 sensor on nvlog.
  Created by Pablo Mariño Boga, January 20, 2022.
*/

#ifndef __TemperatureSensor_h
#define __TemperatureSensor_h

#include <Arduino.h>
#include <Adafruit_AHTX0.h>

class TemperatureSensor
{

private:
  Adafruit_AHTX0 aht;

public:
  TemperatureSensor();
  void begin();
  float *getSample();
};

#endif