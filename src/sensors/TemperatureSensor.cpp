#include "TemperatureSensor.h"

Adafruit_AHTX0 aht;

TemperatureSensor::TemperatureSensor()
{
  
}

void TemperatureSensor::begin(){
  if (!aht.begin())
    Serial.println("Could not find AHT? Check wiring");
}

float *TemperatureSensor::getSample()
{
  static float result[2];
  sensors_event_t humidity, temp;
  if(aht.getEvent(&humidity, &temp)){
    result[0] = temp.temperature;
    result[1] = humidity.relative_humidity;
  }else{
    result[0] = 0.0;
    result[1] = 0.0;
    
  }
  return result;
}