/*
  Sound.h - Library for interfacing GY-SPH0645 sensor on nvlog.
  Created by Pablo Mariño Boga, June 23, 2021.
*/
#ifndef __SoundSensor_h
#define __SoundSensor_h

#define BUFSIZE 3072//1024

#include <Arduino.h>
#include <Adafruit_ZeroI2S.h>

class SoundSensor
{

private:
  int32_t *left;
  int32_t *right;
  Adafruit_ZeroI2S i2s;

public:
  SoundSensor();
  void begin();
  void getSample();
};

#endif